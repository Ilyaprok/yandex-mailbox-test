#ifndef MAIL_BOX_H
#define MAIL_BOX_H


#include "IMailBox.h"
#include <atomic>



template <typename T>
class MailBox1Sender1Receiver: public IMailBox<T>
{
public:	
	MailBox1Sender1Receiver<T>(){
		ptr_read = 0;
		ptr_write = 0;
	}

	~MailBox1Sender1Receiver() {}

	virtual bool IsEmpty() {
		return !buffer[ptr_read].has_value();
	}

	virtual bool IsFull() {
		return buffer[ptr_write].has_value();			
	}

	virtual ErrorCode SendData(const T &data) {

		//проверяем, если данные в данном элементе валидны, значит мы его еще не считали, значит свободное место кончилось
		if (buffer[ptr_write].has_value())
			return ErrorCode::ERROR;

		//записываем данные, инкрементируем указатель на следующий свободный для записи элемент
		buffer[ptr_write] = data;
		//не уверен, надо ли это делать в данной ситуации, на всякий случай сделаю
		//используем барьер памяти, чтобы сначала записать в буфер, а потом только инкрементировать указатель
		ptr_write.store((ptr_write + 1) % MAIL_BOX_SIZE);

		return ErrorCode::OK;
	}

	virtual ErrorCode GetData(optional<T> &data) {

		//если текущий указатель указывает на элемент у которого нет данных, значит очередь пуста
		if (!buffer[ptr_read].has_value()) {
			data.reset();
			return ErrorCode::ERROR;
		}
		//читаем данные, чистим элемент и инкрементируем указатель на следующий элемент для чтения
		data = move(buffer[ptr_read]);
		buffer[ptr_read].reset();
		//не уверен, надо ли это делать в данной ситуации, на всякий случай сделаю
		//используем барьер памяти, чтобы сначала записать в буфер, а потом только инкрементировать указатель
		ptr_read.store((ptr_read + 1) % MAIL_BOX_SIZE);

		return ErrorCode::OK;
	}
private:
	optional<T> buffer[MAIL_BOX_SIZE];
	//использованы атомики для барьера памяти, не уверен, что они здесь нужны, но на всякий случай сделал
	atomic<uint32_t> ptr_write;
	atomic<uint32_t> ptr_read;

};


#endif

