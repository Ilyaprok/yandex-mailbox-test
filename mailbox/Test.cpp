#include <iostream>
#include <thread>

#define MAIL_BOX_SIZE	5
#include "MailBox1Sender1Receiver.hpp"



void SendQueue(MailBox1Sender1Receiver<int> *queue) {
	int right_vals = 0;
	for (int i = 0; i < 10000; i++) {
		if (queue->SendData(right_vals) == ErrorCode::OK) {
			right_vals++;
			//cout << "SEND right_vals: " << right_vals << endl;

			if (rand()% MAIL_BOX_SIZE == 0)
				this_thread::sleep_for(std::chrono::milliseconds(1));			
		}
		else {
			this_thread::sleep_for(std::chrono::milliseconds(1));
		}
			
		//this_thread::sleep_for(std::chrono::milliseconds(2));
	}
}
void RecQueue(MailBox1Sender1Receiver<int> *queue) {
	int right_vals = 0;
	for (int i = 0; i < 10000; i++) {
		optional<int> ii;
		if (queue->GetData(ii) == ErrorCode::OK) {
			if (right_vals != ii.value())
				cout << "error expected: " << right_vals << " real: " << ii.value() << endl;
			else {
				//cout << "RECEIVE expected: " << right_vals << " real: " << ii.value() << endl;
			}
			right_vals++;
			if (rand() % MAIL_BOX_SIZE == 0)
				this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		else {
			this_thread::sleep_for(std::chrono::milliseconds(1));
		}
		//this_thread::sleep_for(std::chrono::milliseconds(1));
	}
}


int main()
{

	//unit test
	MailBox1Sender1Receiver<string> queue;

	auto r1 = queue.IsEmpty();
	auto r2 = queue.IsFull();

	if (r1 != true || r2 != false)
		return -1;

	queue.SendData("test1");

	auto r3 = queue.IsEmpty();
	auto r4 = queue.IsFull();

	if (r3 != false || r3 != false)
		return -2;

	optional<string> i1;
	auto g1 = queue.GetData(i1);

	if (g1 != ErrorCode::OK || i1.value() != "test1")
		return -3;

	auto r5 = queue.IsEmpty();
	auto r6 = queue.IsFull();


	if (r5 != true || r6 != false)
		return -4;

	if (queue.SendData("test1") != ErrorCode::OK)
		return -5;
	if (queue.SendData("test2") != ErrorCode::OK)
		return -6;
	if (queue.SendData("test3") != ErrorCode::OK)
		return -7;
	if (queue.SendData("test4") != ErrorCode::OK)
		return -8;
	if (queue.SendData("test5") != ErrorCode::OK)
		return -9;

	auto r7 = queue.IsEmpty();
	auto r8 = queue.IsFull();


	if (r7 != false || r8 != true)
		return -10;

	if (queue.SendData("test6") != ErrorCode::ERROR)
		return -11;

	if (queue.GetData(i1) != ErrorCode::OK)
		return -12;
	if (!i1.has_value())
		return -13;
	if (i1.value() != "test1")
		return -14;


	if (queue.GetData(i1) != ErrorCode::OK)
		return -15;
	if (!i1.has_value())
		return -16;
	if (i1.value() != "test2")
		return -17;


	if (queue.GetData(i1) != ErrorCode::OK)
		return -18;
	if (!i1.has_value())
		return -19;
	if (i1.value() != "test3")
		return -20;


	if (queue.GetData(i1) != ErrorCode::OK)
		return -21;
	if (!i1.has_value())
		return -22;
	if (i1.value() != "test4")
		return -23;


	if (queue.GetData(i1) != ErrorCode::OK)
		return -24;
	if (!i1.has_value())
		return -25;
	if (i1.value() != "test5")
		return -26;


	if (queue.GetData(i1) != ErrorCode::ERROR)
		return -27;
	if (i1.has_value())
		return -28;


	auto r9 = queue.IsEmpty();
	auto r10 = queue.IsFull();

	if (r9 != true || r10 != false)
		return -29;



	//простая проверка, что соблюдается последовательность в типичном применении такой очереди
	//не гарантирует, что все правильно, скорее показывает, что в первом приближении работает
	MailBox1Sender1Receiver<int> queue2;

	thread t1 = thread(SendQueue, &queue2);
	thread t2 = thread(RecQueue, &queue2);

	t1.join();
	t2.join();

    std::cout << "tests success!\n"; 
}


