#ifndef IMAIL_BOX_H
#define IMAIL_BOX_H

#include <optional>

using namespace std;

enum class ErrorCode {
	OK,
	ERROR
};

template <typename T>
class IMailBox {
public:
	virtual bool IsEmpty() = 0;
	virtual bool IsFull() = 0;
	virtual ErrorCode SendData(const T &data) = 0;
	virtual ErrorCode GetData(optional<T> &data) = 0;
};

#endif // !IMAIL_BOX_H